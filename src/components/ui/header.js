import React from "react";
import logo from "../../img/logo.png";
const Header = () => {
  return (
    <div>
      <header className="center">
        <img src={logo} alt="Breaking Bad" />
      </header>
    </div>
  );
};

export default Header;
